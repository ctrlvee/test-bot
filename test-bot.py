# py -3 test-bot.py

import configparser
import discord

client = discord.Client()

@client.event
async def on_connect():
    print('We have connected.')

@client.event
async def on_disconnect():
    print('We have disconnected')



@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    print('your message: {0.content}'.format(message))
    print('User: {0.author}'.format(message))
    if message.author == client.user:
        return

    if str(message.author) == "ctrl-vee#8271":
        await message.channel.send('Hi Vee')

    if message.content.startswith('?bot'):
        await message.channel.send('you entered the command prefix')

    if message.content.startswith('Hello'):
        await message.channel.send('Hello!')

parser = configparser.ConfigParser()
parser.read('config.ini')
token = parser.get('bot_config', 'token')

client.run(token)